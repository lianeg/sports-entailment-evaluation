# Sports Entailment Evaluation

This repository contains the evaluation scripts / resources for the [Sports Entailment Dataset](https://gitlab.com/lianeg/temporal-entailment-sports-dataset) that were used in the following paper:

**Blindness to Modality Helps Entailment Graph Mining**. Liane Guillou*, Sander Bijl de Vroe*, Mark Johnson, and Mark Steedman. In Proceedings of the Second Workshop on Insights from Negative Results in NLP @ EMNLP 2021.

## Instructions

To evaluate the performance of an entailment graph constructed using [entGraph](https://github.com/mjhosseini/entGraph) on the Sports Entailment Datataset, please follow these steps:

1) Clone this repository.

2) Install python3 and the following packages:

- sklearn
- numpy
- matplotlib
- tqdm

`pip install -r requirements.txt`

3) cd to the sports-entailment-evaluation directory.

4) Clone the [Sports Entailment Dataset](https://gitlab.com/lianeg/temporal-entailment-sports-dataset).

`git clone https://gitlab.com/lianeg/temporal-entailment-sports-dataset`

5) Run graph_eval.py to evaluate performance of the local organization#organization type-pair graph against the Sports Entailment Dataset. Repeat as necessary for other entailment graphs mined using different parameter settings. E.g. baseline settings, modal-aware, etc.

`python3 scripts/graph_eval.py --entPairFile temporal-entailment-sports-dataset/sports_entailment_pairs.txt --metricsListFile resources/metrics_list.txt --inputDirectory example/graphA/ --experimentName graphA --graphFileExtension binc --typePair organization#organization --dirPrecRecCoords example/results/`

`python3 scripts/graph_eval.py --entPairFile temporal-entailment-sports-dataset/sports_entailment_pairs.txt --metricsListFile resources/metrics_list.txt --inputDirectory example/graphB/ --experimentName graphB --graphFileExtension binc --typePair organization#organization --dirPrecRecCoords example/results/`

6) Run plots.py to generate the precision / recall plot and compute the AUC score for each graph on the _base_ section of the Sports Entailment Dataset.

`python3 scripts/plots.py --inputDirectory example/results/_dis1_dis0 --outputFile pr_rec.png`


## Need Help?

Please direct any queries regarding the dataset to the first authors:

Liane Guillou: liane.guillou@ed.ac.uk \
Sander Bijl de Vroe: sbdv@ed.ac.uk
