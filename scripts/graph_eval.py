#!/usr/bin/env python

##############################################################
# Script to evaluate entailment graph performance on the
# Sports Entailment Dataset:
# https://gitlab.com/lianeg/temporal-entailment-sports-dataset
#
# Liane Guillou and Sander Bijl de Vroe
# The University of Edinburgh
##############################################################

from __future__ import division
from sklearn import metrics

import argparse
import glob
import os
from tqdm import tqdm

class bcolors:
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'


# In the majority of cases predicates will be formatted as pred.1,pred.2
# however, some exceptions exist. For example we want lead.2,lead.over.2
# due to an artefect in our relation extraction pipeline
# E.g ((lead.2,lead.over.2)::Chicago_Blackhawks::San_Jose_Sharks
# from: NHL Roundup: Patrick Kane leads Blackhawks over Sharks 5-3
predicate_exceptions = {"lead over": "({}.2,{}.2)"}


def read_entailment_pairs(entPairFileName):
    entPairStringsToId = {}
    entPairLabels = []
    with open(entPairFileName) as f:
        counter = 0
        for line in f:
            ep = line.split('\t')
            # Right-hand side predicate may contain a preposition,
            # left-hand side predicate *should* never contain a preposition
            # e.g. (win.1,win.against.2) or (lose.1,lose.to.2)
            label = int(ep[2].rstrip('\n'))
            e0left = ep[0].split(" ")[0]
            e0right = ep[0].replace(" ", ".")
            e1left = ep[1].split(" ")[0]
            e1right = ep[1].replace(" ", ".")
            # Argument slot order when considering combining left and right predicates into binaries: 1,2,3,2a
            # If predicates are the same on left and right side, slot pairs could be: 1-2 1-3 2-3 3-2
            # If predicates are different on left and right side (i.e. because there is a preposition),
            # slot pairs could be: 1-1(?) 1-2 1-3 2-2 2-3 3-3(?) 3-2
            # For our purposes we only consider a predicate.1,predicate.2 or possibly a predicate.2,predicate.preposition.2
            #   (pass)
            epString = "({}.1,{}.2)|({}.1,{}.2)".format(e0left, e0right, e1left, e1right)
            if ep[0] in predicate_exceptions:
                ex = predicate_exceptions[ep[0]]
                epString = (ex+"|({}.1,{}.2)").format(e0left, e0right, e1left, e1right)
            elif ep[1] in predicate_exceptions:
                ex = predicate_exceptions[ep[1]]
                epString = ("({}.1,{}.2)|"+ex).format(e0left, e0right, e1left, e1right)
            elif ep[0] in predicate_exceptions and ep[1] in predicate_exceptions:
                ex0 = predicate_exceptions[ep[0]]
                ex1 = predicate_exceptions[ep[1]]
                epString = (ex0 + "|" + ex1).format(e0left, e0right, e1left, e1right)
            if epString in entPairStringsToId:
                print("Exiting program. Duplicate entailment pair found in entailment pairs file: " + entPairFileName)
                exit(1)
            entPairStringsToId[epString] = counter
            entPairLabels.append(label)
            counter += 1
    return entPairStringsToId, entPairLabels


def read_metrics_list(fileName, numEntPairs):
    metrics = {}
    with open(fileName) as f:
        for line in f:
            metric_name = line.split('\t')[0]
            if metric_name != "Metric_name":
                metrics[metric_name] = [0] * numEntPairs
    return metrics


def read_graph_files(args, metricEntPairScores, entPairStrings):
    readPredQ = False
    if args.typePair:
        listFiles = []
        listAllFiles = glob.glob(args.inputDirectory + "/*_" + args.graphFileExtension + ".txt")
        for typePair in args.typePair.split(","):
            filePath = args.inputDirectory + "/" + typePair + "_" + args.graphFileExtension + ".txt"
            filePath = filePath.replace("//","/")
            if filePath in listAllFiles:
                listFiles.append(filePath)
    else:
        listFiles = glob.glob(args.inputDirectory + "/*_" + args.graphFileExtension + ".txt")
    for fileName in listFiles:
        metric_name = None
        num_lines = sum(1 for line in open(fileName))
        with open(fileName) as f:
            for line in tqdm(f, total=num_lines):
                line = line.rstrip("\n")
                if line[:9] == "predicate":
                    predP = line.split(" ")[1].split("#")[0]
                elif line in metricEntPairScores:
                    metric_name = line
                    readPredQ = True
                elif readPredQ == True:
                    if line == "":
                        readPredQ = False
                    else:
                        predQ = line.split("#")[0]
                        score = line.split(" ")[1]
                        predPQ = predP + "|" + predQ
                        if predPQ in entPairStrings and metric_name in metricEntPairScores:
                            i = entPairStrings[predPQ]
                            metricEntPairScores[metric_name][i] = 0 if score == 'NaN' else float(score)
    return metricEntPairScores


def apply_threshold(raw_scores, threshold):
    predictions = [1 if x > threshold else 0 for x in raw_scores]
    return predictions


def evaluate_at_multiple_thresholds(fileName, scores, labels):
    (precision, recall, thresholds) = metrics.precision_recall_curve(labels, scores)
    precrec_lines = [str(p)+" "+str(r)+" "+str(t)+"\n" for p,r,t in zip(precision,recall,thresholds)]
    
    auc = metrics.average_precision_score(labels, scores)
    
    with open(fileName, 'w') as f:
        f.write("auc: " + str(auc) + '\n')
        f.writelines(precrec_lines)


def write_output(dirPrecRecCoords, entPairStrings, entPairScores, entPairLabels, experimentName):
    for metric_name in entPairScores:
        # If the output folder does not exist, create it
        if not os.path.exists(dirPrecRecCoords):
            os.makedirs(dirPrecRecCoords)

        # Get auc score, get precision and recall at multiple thresholds
        output_file_name = metric_name.replace(" ", "_") + ".txt"
        results_file_path = os.path.join(dirPrecRecCoords, "coords_" + experimentName + "_" + output_file_name)
        evaluate_at_multiple_thresholds(results_file_path, entPairScores[metric_name], entPairLabels)


def separate_entailment_subsets(epStringsToId, epLabels, epMetricToScores):
    results = {"dis0": {"epStringsToId": {}, "epLabels": [], "epMetricToScores": {}},
               "dis1": {"epStringsToId": {}, "epLabels": [], "epMetricToScores": {}},
               "dir0": {"epStringsToId": {}, "epLabels": [], "epMetricToScores": {}},
               "par1": {"epStringsToId": {}, "epLabels": [], "epMetricToScores": {}}}
    lookup = {0: "dis0", 1: "dis1", -1: "dir0", -2: "par1"}
    for epStr in epStringsToId:
        epID = epStringsToId[epStr]
        subset = lookup[epLabels[epID]]
        results[subset]["epLabels"].append(epLabels[epID])
        results[subset]["epStringsToId"][epStr] = len(results[subset]["epLabels"])-1
    for subgroup in results:
        results[subgroup]["epMetricToScores"] = {key:[] for (key,value) in epMetricToScores.items()}
    for metric_name in epMetricToScores:
        epToScores = epMetricToScores[metric_name]
        for i in range(0,len(epToScores)):
            subset = lookup[epLabels[i]]
            results[subset]["epMetricToScores"][metric_name].append(epToScores[i])
    return results


def combine_subsets(dirPrecRecCoords, ent_subsets, list_subsets):
    dirName = dirPrecRecCoords
    entPairLabels = []
    epStringsToId = {}
    epMetricToScores = {key:[] for (key,value) in ent_subsets[list_subsets[0]]["epMetricToScores"].items()}
    epStringsToIdOffset = 0
    for s in list_subsets:
        # Labels
        entPairLabels += ent_subsets[s]["epLabels"]
        # ent-pair strings to (list) IDs
        for epString in ent_subsets[s]["epStringsToId"]:
            epStringsToId[epString] = ent_subsets[s]["epStringsToId"][epString] + epStringsToIdOffset
        epStringsToIdOffset = len(epStringsToId)
        # Metric scores
        for metric_name in epMetricToScores:
            epMetricToScores[metric_name] += ent_subsets[s]["epMetricToScores"][metric_name]
        dirName += "_" + s
    entPairLabelsBinary = [1 if x == -2 or x == 1 else 0 for x in entPairLabels]
    return dirName, epStringsToId, epMetricToScores, entPairLabelsBinary


if __name__ == '__main__':

    # Initialise the argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--entPairFile", help="Entailment pairs file (text format)")
    parser.add_argument("-m", "--metricsListFile", help="Metrics list file (text format)")
    parser.add_argument("-i", "--inputDirectory", help="Input directory")
    parser.add_argument("-n", "--experimentName", help="Name of the experiment, e.g. graphA")
    parser.add_argument("-g", "--graphFileExtension", help="Type of graph to evaluate. Options: [sim, binc, etc.]")
    parser.add_argument("-tp", "--typePair", help="List of Type pairs, e.g. organization#organization,person#thing")
    parser.add_argument("-d", "--dirPrecRecCoords", help="Directory to store files with precision-recall coordinates")

    # Read arguments from the command line
    args = parser.parse_args()
    
    # Read in the entailment pairs from JSON file
    entPairStringsToId, entPairLabels = read_entailment_pairs(args.entPairFile)

    # Read in the list of metrics (write a dictionary with metrics as keys)
    metricUnsetEntPairScores = read_metrics_list(args.metricsListFile, len(entPairLabels))

    # Loop over graph files
    metricEntPairScores = read_graph_files(args, metricUnsetEntPairScores, entPairStringsToId)

    # Separate out the entailment subsets:
    # disjunctive 0 = 0
    # disjunctive 1 = 1
    # directional 0 = -1
    # paraphrase 1 = -2
    ent_subsets = separate_entailment_subsets(entPairStringsToId, entPairLabels, metricEntPairScores)

    # Output the coordinates and scores files for each of the following ent_subsets combinations:
    # Experiments
    experiments = [
        ["dis1", "dis0"], # Base
        ["dis1", "par1", "dis0", "dir0"], # All
        ["dis1", "dir0"]] # Directional

    for exp in experiments:
        dirName, epStringsToId, epScores, epLabels = combine_subsets(args.dirPrecRecCoords, ent_subsets, exp)
        write_output(dirName, epStringsToId, epScores, epLabels, args.experimentName)