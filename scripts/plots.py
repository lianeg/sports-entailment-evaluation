#!/usr/bin/env python

##############################################################
# Script to plot AUC curves for entailment graph performance
# on the Sports Entailment Dataset:
# https://gitlab.com/lianeg/temporal-entailment-sports-dataset
#
# This is a modified version of a script of the same name
# by Mohammad Javad Hosseini:
# https://github.com/mjhosseini/entgraph_eval
#
# Liane Guillou and Sander Bijl de Vroe
# The University of Edinburgh
##############################################################

import matplotlib.pyplot as plt
import sys
from sklearn import metrics
import numpy as np
import os
import argparse


def compute_auc_score(xs,ys):
    """
    Compute AUC score given lists of precision/recall (X and Y) values
    :param xs: recall values
    :param ys: precision values
    :return: Area Under the Curve (AUC) score
    """
    ret = 0;
    sortIdx = np.argsort(xs)
    xs = [xs[i] for i in sortIdx]
    ys = [ys[i] for i in sortIdx]

    for i in range(1,len(xs)):
        dx = xs[i]-xs[i-1]
        y = (ys[i]+ys[i-1])/2
        ret += (dx*y);
    return ret


def get_color(idx):
    """
    Get line appearance for plots, with different appearance for each entailment graph / system
    :param idx: unique ID value for each entailment graph for which performance should be plotted
    :return: values for line colour, marker, hatching, and mark of the idx as "used"
    """
    hatch = "-"
    idxUsed = True
    if (idx==0):
        color = 'forestgreen'
        marker = 'x'
    elif (idx == 1):
        color = 'darkviolet'
        marker = 'D'
    elif (idx==2):
        color = 'royalblue'
        marker = '^'
    else:
        color = 'k--'
        marker = 'x'
    return (color, marker, hatch, idxUsed)


def main(root, output_file_name):
    """
    Plot AUC curves for performance of each entailment graph
    :param root: directory path - containing precision/recall files output by graph_eval.py
    :param output_file_name: name of plot file to output
    :return: n/a
    """
    aucs = []

    plt.clf()
    plt.cla()

    fnames = os.listdir(root)
    fnames.reverse()
    f_idx = 0
    usedIdx = 0
    for fname in fnames:
        if not fname.endswith(".txt"):
            continue

        method = fname[:-4]
        f = open(root+fname)
        xs = []
        ys = []
        x2s = []
        y2s = []
        lines = f.read().splitlines()

        best_f1 = 0
        best_pr = 0
        best_rec = 0

        for (i,line) in enumerate(lines):
            if len(line.split())<2:
                continue
            if line.startswith("auc"):
                auc = line.split()[1]
                continue

            mod = 20

            ss = line.split()
            pr = float(ss[0])
            rec = float(ss[1])

            try:
                f1 = 2 * pr * rec / (pr + rec)
            except:
                f1 = 0

            if f1 > best_f1:
                best_f1 = f1
                best_pr = pr
                best_rec = rec

            if pr > .5 and i != len(lines) - 1:
                x2s.append(rec)
                y2s.append(pr)

            if len(lines)<30 or i%mod==0 or i==1 or (pr>.7 and i%(mod/2)==0) or (pr>.8 and i%(mod/3)==0) or (pr>.8 and i%(mod/4)==0) or i>len(lines)-3:
                if pr < 1 and rec < 1:
                    xs.append(rec)
                    ys.append(pr)

        auc = -1
        if (len(x2s)>2):
            auc = metrics.auc(x2s, y2s)
            aucs.append(auc)
        else:
            aucs.append(-3)

        xs = xs[0:len(xs)-1]
        ys = ys[0:len(ys) - 1]
        (color,marker,hatch,idxUsed) = get_color(usedIdx)
        if idxUsed:
            usedIdx += 1

        label = method
        print(label)
        auc = str(auc)
        if auc!="-1":
            print("  auc is: ", auc, method)
            label_score = round(float(auc), 3)
            label += ": " + str(label_score)
        ms = 4
        if len(xs)==1:
            ms = 8
        if len(xs)>1:
            plt.plot(xs, ys, color, label =label, marker=marker, linewidth=1, ms = ms, markevery=2)
        else:
            plt.plot(xs[0], ys[0], color, label=label, marker=marker, ms=ms,linewidth=1,linestyle="None")

        print("  best f1, pr, rec: ")
        print(" ", best_f1, best_pr, best_rec)

        f_idx += 1

    plt.ylim([.4, .7])
    plt.grid(linestyle="--", color="lightgray")
    plt.xlim([0, 1])

    plt.xlabel("Recall", fontsize = 14)
    plt.ylabel("Precision", fontsize = 14)

    handles, labels = plt.gca().get_legend_handles_labels()
    order = np.argsort(aucs)
    order = order[::-1]

    plt.legend([handles[idx] for idx in order], [labels[idx] for idx in order],loc = 1, prop={'size':10})
    plt.savefig(root + output_file_name)


if __name__ == "__main__":

    # Initialise the argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inputDirectory", help="Directory containing precision/recall files")
    parser.add_argument("-o", "--outputFile", help="Plot output file (.png format)")

    # Read arguments from the command line
    args = parser.parse_args()

    root = args.inputDirectory + "/"

    # Check if root path exists
    if not os.path.exists(root):
        print("Directory does not exist: " + root)
        sys.exit(1)

    main(root, args.outputFile)